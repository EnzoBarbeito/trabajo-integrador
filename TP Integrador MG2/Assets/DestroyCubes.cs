using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCubes : MonoBehaviour
{
    private InstanciadorPiedra piedra;

    private void Start()
    {
        piedra = GameObject.Find("InstanciadorPiedra").GetComponent<InstanciadorPiedra>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CuboFulbo"))
            Destroy(collision.gameObject, 5f);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("CuboFulbo"))
            Destroy(other.gameObject, 5f);


    }
}
