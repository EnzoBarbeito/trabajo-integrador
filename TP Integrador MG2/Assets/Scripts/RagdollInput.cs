using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class RagdollInput : MonoBehaviour
{
    [SerializeField] private InputManager input;
    [SerializeField] private RagdollActivator ragdoll;
    [SerializeField] private GameManager Manager;
    [SerializeField] private CheckPoint_Manager checkpoint;
    [SerializeField] private Volume volumen;
    [SerializeField] private ColorAdjustments color;
    [SerializeField] private LensDistortion lens;

    void Start()
    {
        checkpoint = GameObject.Find("--- Game Manager ---").GetComponent<CheckPoint_Manager>();
        Manager = GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();
        ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
        input = GetComponent<InputManager>();
        volumen = GameObject.Find("Global Volume").GetComponent<Volume>();

    }

    void Update()
    {
        Input();
    }


    private void Input()
    {
        if (input.RagdollTrigger == true )
        {
            ragdoll.ragdollActivo = true;
        }
        if (input.RagdollTriggerOFF == true && ragdoll.ragdollActivo == true)
        {
            ragdoll.ragdollActivo = false;

        }

        if(input.CameraView==true)
        {
            Manager.camara1ST = true;
        }
        if (input.CameraViewOFF == true)
        {
            Manager.camara1ST = false;
        }


        if (input.Respawn == true)
        {
            volumen.profile.TryGet<ColorAdjustments>(out color);
            volumen.profile.TryGet<LensDistortion>(out lens);
            color.active = false;
            lens.active = false;
            checkpoint.CheckPoints();
        }

    }

}
