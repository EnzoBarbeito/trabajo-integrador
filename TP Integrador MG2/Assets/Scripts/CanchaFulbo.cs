using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanchaFulbo : MonoBehaviour
{
    [SerializeField] private Material skybox;
    [SerializeField] private GameObject puerta;
    void Start()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RenderSettings.skybox = skybox;
            Animator anim = puerta.GetComponent<Animator>();
            anim.SetTrigger("puertaCancha");
        }
    }
}
