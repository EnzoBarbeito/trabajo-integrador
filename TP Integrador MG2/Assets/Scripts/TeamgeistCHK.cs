using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamgeistCHK : MonoBehaviour
{
    private short decoy;
    [Header("--- Checkpoints Pelota ---\n")]
    [SerializeField] private Transform[] puntosSpawn;
    [Header("--- Checkpoints Pelota ---\n")]
    [SerializeField] private bool CHK0 = true;
    [SerializeField] private bool CHK1 = false;
    [SerializeField] private bool CHK2 = false;
    [SerializeField] private bool CHK3 = false;
    [SerializeField] private bool CHK4 = false;
    [SerializeField] private bool CHK5 = false;
    [SerializeField] private bool CHK6 = false;
    [Header("--- Scripts Necesarios ---\n")]
    [SerializeField] private PauseManager pausa;
    [SerializeField] private GameManager Manager;

    private void Start()
    {
        pausa = GameObject.Find("--- UI Manager ---").GetComponent<PauseManager>();
        Manager = GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();

    }




    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Pu�o")) //CHK2
        {
            CHK0 = false;
            CHK1 = true;
        }
        if (other.gameObject.CompareTag("CHK3"))
        {
            CHK1 = false;
            CHK2 = true;
        }
        if (other.gameObject.CompareTag("CHK4"))
        {
            CHK2 = false;
            CHK3 = true;
        }
        if (other.gameObject.CompareTag("CHK6"))
        {
            CHK3 = false;
            CHK4 = true;
        }
        if (other.gameObject.CompareTag("CHK7"))
        {
            CHK4 = false;
            CHK5 = true;
        }
        if (other.gameObject.CompareTag("chk8"))
        {
            CHK5 = false;
            CHK6 = true;
        }

        if (other.gameObject.CompareTag("RagdollWater")) // respawner tras caer al agua.
        {
            if (CHK0 == true)
            {
                gameObject.transform.position = puntosSpawn[0].transform.position;
            }
            if (CHK1 == true)
            {
                gameObject.transform.position = puntosSpawn[1].transform.position;
            }
            if (CHK2 == true)
            {
                gameObject.transform.position=puntosSpawn[2].transform.position;
            }
            if (CHK3 == true)
            {
                gameObject.transform.position = puntosSpawn[3].transform.position;
            }
            if (CHK4 == true)
            {
                gameObject.transform.position = puntosSpawn[4].transform.position;
            }
            if (CHK5 == true)
            {
                gameObject.transform.position = puntosSpawn[5].transform.position;
            }
            if (CHK6 == true)
            {
                gameObject.transform.position = puntosSpawn[6].transform.position;
            }

        }

        if (other.gameObject.CompareTag("TimerEnd"))
        {
            Manager.terminoTimer = true;
            Manager.camaraAlt = true;
            pausa.Win = true;

        }
    }

}
