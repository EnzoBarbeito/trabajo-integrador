using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint_Manager : MonoBehaviour
{
    [SerializeField] private Transform[] checkpoints;
    [SerializeField] private Transform transformJugador;
    [SerializeField] internal bool checkP0;
    [SerializeField] internal bool checkP1;
    [SerializeField] internal bool checkP2;
    [SerializeField] internal bool checkP3;
    [SerializeField] internal bool checkP4;
    [SerializeField] internal bool checkP5;
    [SerializeField] internal bool checkP6;
    [SerializeField] internal bool checkP7;
    [SerializeField] internal bool checkP8;
    [SerializeField] internal bool checkP9;
    [SerializeField] private RagdollActivator ragdoll;
    [SerializeField] private ControlJugador jugador;

    void Start()
    {
        jugador = GameObject.Find("PlayerArmature").GetComponent<ControlJugador>();
        ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
        transformJugador = GameObject.Find("PlayerArmature").GetComponent<Transform>();
        checkP0 = true; checkP1 = false; checkP2 = false;  checkP3 = false; checkP4 = false; checkP5 = false; checkP6 = false; checkP7 = false; checkP8 = false; checkP9 = false;

    }
    private void Update()
    {
        AutomaticCheck();
    }
    internal void CheckPoints()
    {
        if (checkP0==true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[0].position;
        }
            
        if (checkP1==true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[1].position;
        }
        if (checkP2==true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[2].position;
        }
        if (checkP3 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[3].position;
        }
        if (checkP4 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[4].position;
        }
        if (checkP5 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[5].position;
        }
        if (checkP6 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[6].position;
        }
        if (checkP7 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[7].position;
        }
        if (checkP8 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[8].position;
        }
        if (checkP9 == true)
        {
            jugador.estaEnPiso = true;
            jugador.tiempoEnAire = 0;
            ragdoll.ragdollActivo = false;
            transformJugador.position = checkpoints[9].position;
        }
    }

    private void AutomaticCheck()
    {

        if(checkP1==true)
            checkP0 = false;
        if (checkP2 == true)
            checkP1 = false;
        if (checkP3 == true)
            checkP2 = false;
        if (checkP4 == true)
            checkP3 = false;
        if (checkP5 == true)
            checkP4 = false;
        if (checkP6 == true)
            checkP5 = false;
        if (checkP7 == true)
            checkP6 = false;
        if (checkP8 == true)
            checkP7 = false;
        if (checkP9 == true)
            checkP8 = false;
    }
}
