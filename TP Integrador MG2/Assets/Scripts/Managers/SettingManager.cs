using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManager : MonoBehaviour
{
    private static SettingManager instancia;
    [Header("---- Customizacion Camara----\n")]
    public bool camaraAlt;
    public bool camara1ST;
    [Header("---- Timer ----\n")]
    internal bool comienzoTimer;
    internal bool turnoJ1;
    internal float tiempoJ1;
    internal float tiempoFinalJ1;
    private float tiempoFinalJ2;
    [Header("---- Customizacion Jugador ----\n")]
    [SerializeField] internal string Nombre;
    [SerializeField] internal Color colorElegido;
    [Header("---- Colores ----\n")]
    [SerializeField] internal bool Rojo=false;
    [SerializeField] internal bool Azul=false;
    [SerializeField] internal bool Amarillo=false;
    [SerializeField] internal bool Verde=false;
    [SerializeField] internal bool Blanco = false;
    [SerializeField] internal bool Negro = false;
    [Header("---- Desafio F ----\n")]
    [SerializeField] internal bool fcActivo = false;


    private void Awake()
    {
   
       DontDestroyOnLoad(gameObject);

    }

    void Start()
    {
        comienzoTimer = false;
        turnoJ1 = true;
        camaraAlt = false;
        camara1ST = false;
        Nombre = "";
        Cursor.lockState=CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDeJuegoJ1();
        ElegirColor();
    }
    private void tiempoDeJuegoJ1()
    {
        if (turnoJ1 == true && comienzoTimer==true)
        {
            tiempoJ1 += 1*Time.deltaTime;

        }
    }

    private void CambiodeMaterial()
    {

    }

    private void ElegirColor()
    {
        if (Rojo)
        {
            colorElegido = new Color(1f, 0f, 0f);
        }
        if (Azul)
        {
            colorElegido = new Color(0f, 0f, 1f);
        }
        if (Verde)
        {
            colorElegido = new Color(0f, 1f, 0f);
        }
        if (Amarillo)
        {
            colorElegido = new Color(1f, 1f, 0f);
        }
        if (Blanco)
        {
            colorElegido = new Color(1f, 1f, 1f);
        }
        if (Negro)
        {
            colorElegido = new Color(0f, 0f, 0f);
        }
    }
}
