using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("---- Customizacion Camara----\n")]
    public bool camaraAlt;
    public bool camara1ST;
    [Header("---- Timer ----\n")]
    [SerializeField] internal bool comienzoTimer;
    [SerializeField] internal bool turnoJ1;
    [SerializeField] internal float tiempoJ1;
    [SerializeField] internal bool terminoTimer;
    [SerializeField] internal float tiempoFinalJ1;
    private float tiempoFinalJ2;
    [Header("---- Customizacion Jugador ----\n")]
    [SerializeField] internal string PlayerNombre;
    [SerializeField] private Material matPiernas;
    [SerializeField] private Material matPechoCab;
    [SerializeField] private Material matBrazos;
    [SerializeField] private Renderer rndrd;
    [SerializeField] internal bool cambiarColor;
    [SerializeField] private Color colorElegido;
    [Header("---- Colores ----\n")]
    [SerializeField] internal bool Rojo=false;
    [SerializeField] internal bool Azul=false;
    [SerializeField] internal bool Amarillo=false;
    [SerializeField] internal bool Verde=false;
    [SerializeField] internal bool Blanco = false;
    [SerializeField] internal bool Negro = false;
    [Header("---- Settings ----\n")]
    [SerializeField] private SettingManager SettManager;
    [Header("---- PuzzlePelota ----\n")]
    [SerializeField] internal bool abrirPuerta = false;
    [SerializeField] internal bool arcoGol1 = false;
    [SerializeField] internal bool arcoGol2 = false;
    [SerializeField] internal bool arcoGol3 = false;
    [SerializeField] private Animator anim;
    [Header("---- FutbolChallenge ----\n")]
    [SerializeField] internal bool ChallengeActivo;
    [SerializeField] internal GameObject trackChallenge;




    void Start()
    {
        rndrd = GameObject.Find("Armature_Mesh").GetComponent<Renderer>();
        comienzoTimer = false;
        terminoTimer = false;
        turnoJ1 = true;
        camara1ST = false;
        cambiarColor = true;
        Cursor.visible=true;
        SettManager = GameObject.Find("---SETTINGS MANAGER---").GetComponent<SettingManager>();
        camaraAlt = SettManager.camaraAlt;
        ChallengeActivo = SettManager.fcActivo;
        PlayerNombre = SettManager.Nombre.ToUpper();
        trackChallenge = GameObject.Find("---OBS_SPECIALCHALLENGE---");
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDeJuegoJ1();
        CambiodeMaterial();
        ElegirColor();
        abrirPuertaArco();
        AnimacionPuerta();
        DesafioFutbol();
    }
    private void tiempoDeJuegoJ1()
    {
        if (turnoJ1 == true && comienzoTimer == true)
        {
            tiempoJ1 += 1 * Time.deltaTime;

        }
        if (terminoTimer == true)
        {
            turnoJ1 = false; comienzoTimer = false;
            tiempoFinalJ1 = tiempoJ1;
        }
    
    }

    private void CambiodeMaterial()
    {
        if (cambiarColor==true)
        {

            matBrazos = rndrd.sharedMaterials[0];
            matBrazos.color = SettManager.colorElegido;
            matPechoCab = rndrd.sharedMaterials[1];
            matPechoCab.color = SettManager.colorElegido;
            matPiernas = rndrd.sharedMaterials[2];
            matPiernas.color = SettManager.colorElegido;
            cambiarColor = false;

        }
    }

    private void ElegirColor()
    {
        if (Rojo)
        {
            colorElegido = new Color(1f, 0f, 0f);
        }
        if (Azul)
        {
            colorElegido = new Color(0f, 0f, 1f);
        }
        if (Verde)
        {
            colorElegido = new Color(0f, 1f, 0f);
        }
        if (Amarillo)
        {
            colorElegido = new Color(1f, 1f, 0f);
        }
        if (Blanco)
        {
            colorElegido = new Color(1f, 1f, 1f);
        }
        if (Negro)
        {
            colorElegido = new Color(0f, 0f, 0f);
        }
    }

    private void abrirPuertaArco()
    {
        if (arcoGol1 == true && arcoGol2 == true && arcoGol3 == true)
        {
            abrirPuerta = true;
            anim.SetTrigger("abrirPuerta");

        }
    }

    private void AnimacionPuerta()
    {
        if (abrirPuerta == true)
        {
            anim.SetTrigger("abrirPuerta");

        }
    }

    private void DesafioFutbol()
    {
        if (ChallengeActivo == true)
        {
            trackChallenge.SetActive(true);
        }
        else if (ChallengeActivo == false)
        {
            trackChallenge.SetActive(false);
        }
    }
}
