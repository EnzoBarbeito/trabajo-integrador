using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    [SerializeField] internal GameObject menuPausa;
    [SerializeField] private ControlJugador jugador;
    [SerializeField] private GameManager manager;
    [SerializeField] private GameObject btn1;
    [SerializeField] private GameObject btn2;
    [SerializeField] private GameObject btn3;
    [SerializeField] private GameObject btn4;
    [SerializeField] private GameObject btnToggle;
    [SerializeField] private GameObject btnSlider;
    [SerializeField] private GameObject btnVolver;
    [SerializeField] private GameObject menuOpciones;
    [SerializeField] private GameObject menuVictoria;
    [SerializeField] private TMPro.TMP_Text txtNombreVictoria;
    [SerializeField] private TMPro.TMP_Text txtTiempoFinal;
    [SerializeField] private Toggle camaraALT;
    [SerializeField] private Toggle NKS;
    [SerializeField] private Slider sensSlider;
    [SerializeField] internal bool Win;


    void Start()
    {
        txtNombreVictoria = GameObject.Find("WINNombre").GetComponent<TMPro.TMP_Text>();
        txtTiempoFinal = GameObject.Find("tiempoFinal").GetComponent<TMPro.TMP_Text>();
        menuPausa = GameObject.Find("MenuPausa");
        menuVictoria = GameObject.Find("--Win Menu--");
        jugador = GameObject.Find("PlayerArmature").GetComponent<ControlJugador>();
        manager= GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();
        btn1 = GameObject.Find("BTN_Continuar");
        btn2 = GameObject.Find("BTN_Restart");
        btn3 = GameObject.Find("BTN_Opciones");
        btn4 = GameObject.Find("BTN_Salir");
        camaraALT = GameObject.Find("TGL_CALT").GetComponent<Toggle>();
        NKS = GameObject.Find("TGL_KICK").GetComponent<Toggle>();
        menuOpciones = GameObject.Find("-- Menu_Options --");
        sensSlider = GameObject.Find("Sensitibity").GetComponent<Slider>();
        menuOpciones.SetActive(false);
        menuVictoria.SetActive(false);
        sensSlider.minValue = 0f;
        sensSlider.maxValue = 50f;
        sensSlider.value = 21.9f;
        Win = false;
    }

    void Update()
    {
        CambioCamara();
        SistemaKick();
        Sensitibity();
        if (Win == true)
            CartelVictoria();
    }

    public void Continuar()
    {
        menuPausa.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        jugador.estaEnMenu = false;
    }
    public void Opciones()
    {
        btn1.SetActive(false);
        btn2.SetActive(false);
        btn3.SetActive(false);
        btn4.SetActive(false);
        menuOpciones.SetActive(true);
    }
    public void VolverOpciones()
    {
        menuOpciones.SetActive(false);
        btn1.SetActive(true);
        btn2.SetActive(true);
        btn3.SetActive(true);
        btn4.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
    public void Salir()
    {
        SceneManager.LoadScene(0);
    }
    private void CambioCamara()
    {
        switch (camaraALT.isOn)
        {
            case true: manager.camaraAlt=true;
                break;
            case false:
                manager.camaraAlt = false;
                break;
        }

    }
    private void SistemaKick()
    {
        switch (NKS.isOn)
        {
            case true:
                jugador.nuevoSistemaKick = true;
                break;
            case false:
                jugador.nuevoSistemaKick = false;
                break;
        }

    }
    private void Sensitibity()
    {
        jugador.sensibilidadMouse = sensSlider.value;

    }

    public void CartelVictoria()
    {
        menuPausa.SetActive(true);
        menuOpciones.SetActive(false);
        btn1.SetActive(false);
        btn2.SetActive(false);
        btn3.SetActive(false);
        btn4.SetActive(false);
        menuVictoria.SetActive(true);
        txtNombreVictoria.text = manager.PlayerNombre;
        float minutos = Mathf.FloorToInt(manager.tiempoFinalJ1 / 60);
        float segundos = Mathf.FloorToInt(manager.tiempoFinalJ1 % 60);
        txtTiempoFinal.text = string.Format("{0:00} : {1:00}", minutos, segundos);
        StartCoroutine(VolverMenu());
    }

    IEnumerator VolverMenu()
    {
        yield return new WaitForSecondsRealtime(12f);
        SceneManager.LoadScene(0);
        yield break;
    }
}
