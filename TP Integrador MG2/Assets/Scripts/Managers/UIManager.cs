using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private short decoy;

    [Header("---- Variables Generales -----\n")]
    [SerializeField] private TMPro.TMP_Text textoPiso;
    [SerializeField] private TMPro.TMP_Text textoTimerPiso;
    [SerializeField] private TMPro.TMP_Text textoCNL;
    [SerializeField] private GameObject logoCNL;
    [SerializeField] private Animator anim;
    [Header("---- HUD -----\n")]
    [SerializeField] private TMPro.TMP_Text timerPartida;
    [SerializeField] private TMPro.TMP_Text nombreJugador;
    [SerializeField] private GameObject hudNombre;
    [SerializeField] private GameObject hudNombreFC;
    [SerializeField] private GameObject hudTiempo;
    [Header("---- Conexiones Scritps -----\n")]
    [SerializeField] private RagdollActivator ragdoll;
    [SerializeField] private GameManager manager;
    [SerializeField] private ControlJugador jugador;
    [SerializeField] private PauseManager pausa;

    void Start()
    {
        logoCNL = GameObject.Find("SFIcon");
        textoCNL = GameObject.Find("TextoCNL").GetComponent<TMPro.TMP_Text>();
        textoTimerPiso = GameObject.Find("TimerPiso").GetComponent<TMPro.TMP_Text>();
        textoPiso = GameObject.Find("Texto Piso").GetComponent<TMPro.TMP_Text>();
        ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
        manager= GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();
        jugador= GameObject.Find("PlayerArmature").GetComponent<ControlJugador>();
        anim = GameObject.Find("PlayerArmature").GetComponent<Animator>();
        timerPartida = GameObject.Find("Timer").GetComponent<TMPro.TMP_Text>();
        nombreJugador = GameObject.Find("TXT_NombreJugador").GetComponent<TMPro.TMP_Text>();
        pausa = GameObject.Find("MenuPausa").GetComponent<PauseManager>();

    }

    // Update is called once per frame
    void Update()
    {
        textoTimerPiso.text = jugador.tiempoEnAire.ToString("0");
        pisoUI();
        caidaNoLetalUI();
        TimerJugadores();
        NombreJugador();
        HideHUDName();

    }

    private void pisoUI()
    {
        if (jugador.estaEnPiso==true)
        {
            textoPiso.text = "Esta en el piso";
            jugador.animBoolCheck = false;
            anim.SetBool("estaEnElPiso", true);
            anim.SetBool("caidaNoLetal", false);
        }
        else
        {
            textoPiso.text = "No esta en el piso";
            anim.SetBool("estaEnElPiso", false);
        }
    }

    private void caidaNoLetalUI()
    {
        if (jugador.caidaNoLetal == true)
        {
            textoCNL.text = "Caida No Letal: Activada.";
            anim.SetBool("caidaNoLetal", true);
            logoCNL.SetActive(true);
        }
        else
        {
            textoCNL.text = "Caida No Letal: Desactivada.";
            anim.SetBool("caidaNoLetal", false);
            logoCNL.SetActive(false);

        }

    }

    private void TimerJugadores()
    {
        float minutos = Mathf.FloorToInt(manager.tiempoJ1 / 60);
        float segundos = Mathf.FloorToInt(manager.tiempoJ1 % 60);
        timerPartida.text = string.Format("{0:00} : {1:00}", minutos, segundos);
    }

    private void NombreJugador()
    {
        nombreJugador.text = manager.PlayerNombre;
    }

    private void HideHUDName()
    {
        if (manager.ChallengeActivo == true)
        {
            hudNombre.SetActive(false);
            hudNombreFC.SetActive(true);

        }
        else if (manager.ChallengeActivo == false)
        {
            hudNombreFC.SetActive(false);
            hudNombre.SetActive(true);
        }



    }
}
