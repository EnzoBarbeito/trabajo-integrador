using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonPelota : MonoBehaviour
{
    [SerializeField] private GameObject Pelota;
    [SerializeField] private Transform SpawnPelota;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Apret� boton Spawn Pelota");
            Pelota.transform.eulerAngles = new Vector3(0, 0, 0); 
            Pelota.transform.position = SpawnPelota.position;
        }
    }
}
