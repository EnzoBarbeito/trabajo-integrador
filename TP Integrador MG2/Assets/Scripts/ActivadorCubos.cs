using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorCubos : MonoBehaviour
{
    [SerializeField] private Rigidbody[] cubos;
    internal bool cambioDeEstado=false;
    private void Start()
    {
        cubos= transform.GetComponentsInChildren<Rigidbody>();
    }

    void Update()
    {
        KinematicToSolid();
    }

    private void KinematicToSolid()
    {
        if (cambioDeEstado == true)
            foreach (Rigidbody rb in cubos)
            {
                rb.isKinematic = false;                
            }
    }
}
