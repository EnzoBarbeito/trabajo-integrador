using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BASE_Obstaculos : MonoBehaviour
{
    private RagdollActivator ragdoll;
    public virtual void Start()
    {
        ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
    }
    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ragdoll.ragdollActivo = true;
        }
    }
}
