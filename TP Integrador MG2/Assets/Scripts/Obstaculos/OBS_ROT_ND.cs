using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBS_ROT_ND : MonoBehaviour
{
    [Header("---- Variables del Vector ----\n")]
    [SerializeField] private Vector3 tipogiro;
    [SerializeField] private float velocidadDeGiro;
    [Header("---- Tipo de Giro ----\n")]
    [SerializeField] private bool V3_Forward;
    [SerializeField] private bool V3_Back;
    [SerializeField] private bool V3_Left;
    [SerializeField] private bool V3_Right;
    [SerializeField] private bool V3_Up;


    private void Update()
    {
        cambioGiro();
        Rotar();
    }
    private void Rotar()
    {
        gameObject.transform.Rotate(tipogiro*velocidadDeGiro*Time.deltaTime);
    }
    private void cambioGiro()
    {
        if (V3_Forward)
        {
            tipogiro = Vector3.zero;
            tipogiro = Vector3.forward;
        }
        if (V3_Back)
        {
            tipogiro = Vector3.zero;
            tipogiro = Vector3.back;
        }
            
        if (V3_Left)
        {
            tipogiro = Vector3.zero;
            tipogiro = Vector3.left;
        }
        if (V3_Right)
        {
            tipogiro = Vector3.zero;
            tipogiro = Vector3.right;
        }
        if (V3_Up)
        {
            tipogiro = Vector3.zero;
            tipogiro = Vector3.up;
        }
    }

}
