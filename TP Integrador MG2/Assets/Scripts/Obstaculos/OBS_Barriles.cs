using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBS_Barriles : BASE_Obstaculos
{
    [SerializeField] private Transform posicionInicial;
    [SerializeField] private Transform rotacionInicial;
    [SerializeField] private float timerReset;
    public override void Start()
    {
        base.Start();
        timerReset= 0;
    }
    private void Update()
    {
        timerReset += 1 * Time.deltaTime;
        if (timerReset >= 7)
        {
            StartCoroutine(RegresoDeBarril());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("DestroyBarril"))
        {

        }
    }

    IEnumerator RegresoDeBarril()
    {
        Debug.Log("Se ejecuto la corrutina RegresoDeBarril");
        yield return new WaitForSeconds(1f);
        timerReset = 0;
        //gameObject.transform.position = posicionInicial.transform.position;
        //gameObject.transform.eulerAngles= new Vector3 (-90,0f,0f);
        yield break;
    }

}
