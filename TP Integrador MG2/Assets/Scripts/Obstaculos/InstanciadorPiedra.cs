using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorPiedra : MonoBehaviour
{
    [SerializeField] private GameObject Piedra;
    internal bool caePiedra=false;
    void Start()
    {
        Piedra = GameObject.Find("Piedra");
    }

    // Update is called once per frame
    void Update()
    {
        if (caePiedra == true)
            StartCoroutine(InstanciarPiedra());
    }

    IEnumerator InstanciarPiedra()
    {
        Debug.Log("Se Ejecuto la corrutina de la Piedra");
        var clone = Instantiate(Piedra, transform.position, Piedra.transform.rotation);
        clone.transform.eulerAngles = new Vector3(0, 0f, 0f);
        Destroy(clone,6.5f);
        caePiedra = false;
        yield break;
    }
}
