using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorBarriles : MonoBehaviour
{
    [SerializeField] private GameObject Barriles;
    private bool seInstancio = false;
    void Start()
    {
        Barriles = GameObject.Find("Barril");
    }

    // Update is called once per frame
    void Update()
    {
        if (seInstancio == false)
        StartCoroutine(InstanciarBarriles());
    }
    IEnumerator InstanciarBarriles()
    {
        seInstancio = true;
        yield return new WaitForSeconds(4f);
        var clone= Instantiate(Barriles, transform.position,Barriles.transform.rotation);
        clone.transform.eulerAngles = new Vector3(-90, 0f, 0f);
        Destroy(clone, 10f);
        seInstancio = false;
        yield break;
    }
}
