using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBS_PIña : MonoBehaviour
{
    [SerializeField] internal bool puedeDañar;
    [SerializeField] internal bool pruebaMas;
    [SerializeField] internal bool yaAvanzo;
    [SerializeField] internal bool pruebaMenos;
    [SerializeField] internal float valorRecorrido;
    [SerializeField] internal float fuerzaImpacto;

    void Start()
    {
        valorRecorrido = 2.7f;
        pruebaMas = false;
        pruebaMenos = false;
        yaAvanzo = false;
        puedeDañar = true;
    }

    // Update is called once per frame
    void Update()
    {
        Daño();
        if (pruebaMenos == true )
        {
            Restar();
        }
        if (pruebaMas == true && yaAvanzo == false)
        {
            Sumar();
        }
    }
    private void Sumar()
    {
        MovimientoMas();

    }
    private void Restar()
    {
        MovimientoMenos();

    }

    private void MovimientoMenos()
    {
        transform.position = new Vector3 (transform.position.x-valorRecorrido, transform.position.y, transform.position.z);
        pruebaMenos = false;
    }
    private void MovimientoMas()
    {
        transform.position = new Vector3(transform.position.x + valorRecorrido, transform.position.y, transform.position.z);
        pruebaMas = false;
        yaAvanzo = true;
    }

    private void Daño()
    {
        puedeDañar = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (puedeDañar)
            {
                Rigidbody rb = GameObject.Find("PlayerArmature").GetComponent<Rigidbody>();
                rb.AddForce(Vector3.right * fuerzaImpacto, ForceMode.Impulse);
                RagdollActivator ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
                ragdoll.ragdollActivo = true;
            }
            else
                return;

        }

    }
}
