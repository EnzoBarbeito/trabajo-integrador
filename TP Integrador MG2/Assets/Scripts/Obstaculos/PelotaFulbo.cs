using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaFulbo : MonoBehaviour
{

    public float slipperiness = 0.5f; // Valor que determina la cantidad de resbalamiento

    private Rigidbody objectRigidbody;

    private Vector3 playerTransform;

    [SerializeField] private float fuerzaTiro;
    [SerializeField] private Vector3 direccionDisparo;
    [SerializeField] private bool usarDireccionJugador;
    [SerializeField] private GameManager manager;
         

    private void Start()
    {
        manager = GameObject.Find("--- Game Manager ---").GetComponent <GameManager>();
        playerTransform = new Vector3();
        objectRigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        DireccionJugador();
       
    }

    private void OnCollisionStay(Collision collision)
    {
        // Obtener el vector de velocidad actual del objeto
        Vector3 currentVelocity = objectRigidbody.velocity;

        // Calcular una velocidad modificada con base en la direcci�n del objeto y la slipperiness
        Vector3 modifiedVelocity = currentVelocity - (currentVelocity * slipperiness * Time.deltaTime);

        // Asignar la velocidad modificada al objeto
        objectRigidbody.velocity = modifiedVelocity;

        if (collision.gameObject.CompareTag("Player"))
        {
            ControlJugador player = collision.gameObject.GetComponent<ControlJugador>();

            if (player.estaPateandoF == true)
            {
                 if (usarDireccionJugador == true)
                    objectRigidbody.AddForce(playerTransform * fuerzaTiro, ForceMode.Impulse);
            }
            else
            {
                return;
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ControlJugador player = collision.gameObject.GetComponent<ControlJugador>();

            if (player.estaPateandoF == true)
            {
                if (usarDireccionJugador ==false)
                objectRigidbody.AddForce(direccionDisparo*fuerzaTiro, ForceMode.Impulse);
                else if (usarDireccionJugador==true)
                objectRigidbody.AddForce(playerTransform* fuerzaTiro, ForceMode.Impulse);

            }
            else
            {
                return;
            }
        }
        if (collision.gameObject.CompareTag("CuboFulbo"))
        {
            //Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            //rb.isKinematic = false;
        }
        if (collision.gameObject.CompareTag("ParedFulbo"))
        {
            DestroyImmediate(collision.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ParedFulbo"))
        {
            ActivadorCubos cubos = GameObject.Find("PARED_Cubos").GetComponent<ActivadorCubos>();
            cubos.cambioDeEstado = true;
        }
        if (other.gameObject.CompareTag("ArcoGol"))
        {
            manager.arcoGol1 = true;
        }
        if (other.gameObject.CompareTag("ArcoGol2"))
        {
            manager.arcoGol2 = true;
        }
        if (other.gameObject.CompareTag("ArcoGol3"))
        {
            manager.arcoGol3 = true;
        }

    }

    private void DireccionJugador()
    {
        ControlJugador player = GameObject.Find("PlayerArmature").GetComponent<ControlJugador>();
        playerTransform = player.gameObject.transform.forward;
    }

}
