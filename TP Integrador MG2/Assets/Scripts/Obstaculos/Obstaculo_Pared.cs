using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo_Pared : MonoBehaviour
{
    [SerializeField] private float velocidadRotacion;

    private void Update()
    {
      TransformarRotacion();  
    }

    private void TransformarRotacion()
    {
        
        transform.eulerAngles += new Vector3(0f, 0f, 45f)*Time.deltaTime*velocidadRotacion;

    }

}
