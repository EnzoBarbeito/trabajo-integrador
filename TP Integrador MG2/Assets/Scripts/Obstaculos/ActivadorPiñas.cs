using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorPiñas : MonoBehaviour
{
    [SerializeField] private GameObject[] obstaculosTrompadas;
    [SerializeField] private int numeroRandomizado;
    void Start()
    {

    }

    void Update()
    {
        ActivarObstaculos();
    }

    private void ActivarObstaculos()
    {
        for (int i = 0; i < obstaculosTrompadas.Length; i++)
        {
            OBS_PIña objetoElegidoTrompada = obstaculosTrompadas[i].GetComponent<OBS_PIña>();
            objetoElegidoTrompada.pruebaMas = true;
            objetoElegidoTrompada.puedeDañar = true;


        }
        

    }
}
