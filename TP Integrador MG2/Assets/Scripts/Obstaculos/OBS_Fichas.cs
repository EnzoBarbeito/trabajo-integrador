using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OBS_Fichas : MonoBehaviour
{
    public Transform startPoint; // Punto de inicio
    public Transform endPoint; // Punto de destino
    public float speed = 1f; // Velocidad de movimiento

    private float startTime; // Tiempo de inicio del movimiento

    private void Start()
    {
        // Guardar el tiempo de inicio del movimiento
        startTime = Time.time;
    }

    private void Update()
    {
        // Calcular el tiempo transcurrido desde el inicio del movimiento
        float timeElapsed = Time.time - startTime;

        // Calcular la fracción del recorrido actual mediante un ciclo senoidal
        float t = Mathf.PingPong(timeElapsed * speed, 1f);

        // Calcular la posición actual del objeto mediante interpolación lineal
        transform.position = Vector3.Lerp(startPoint.position, endPoint.position, t);
    }
}
