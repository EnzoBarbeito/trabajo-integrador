using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaculo_Pelota : MonoBehaviour
{
    [SerializeField] private float fuerzaSalto;
    [SerializeField] private ControlJugador jugador;

    private void Start()
    {
        jugador = GameObject.Find("PlayerArmature").GetComponent<ControlJugador>();      
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Animator anim = collision.gameObject.GetComponent<Animator>();
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            rb.AddForce(-rb.velocity.y * Vector3.up, ForceMode.VelocityChange);
            rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
            anim.ResetTrigger("Salto");
        }
    }
}
