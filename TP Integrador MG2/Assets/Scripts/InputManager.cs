using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;

public class InputManager : MonoBehaviour
{

    [Header("----- Variables Globales -----")]

    [SerializeField] private PlayerInput PL;
    public Vector2 Moverse { get; private set; }
    public Vector2 Mirar { get; private set; }
    public bool Correr { get; private set; }
    public bool RagdollTrigger { get; private set; }
    public bool RagdollTriggerOFF { get; private set; }
    public bool Respawn { get; private set; }
    public bool Salto { get; private set; }
    public bool Patada { get; private set; }
    public bool Dive { get; private set; }
    public bool Slide { get; private set; }
    public bool CameraView { get; private set; }
    public bool CameraViewOFF { get; private set; }
    public bool PatadaFulbo { get; private set; }
    public bool Pausar { get; private set; }




    [Header("----- Input Actions -----")]

    private InputActionMap mapaActual;
    private InputAction moverseAccion;
    private InputAction mirarAccion;
    private InputAction correrAccion;
    private InputAction ragdollAccion;
    private InputAction ragdollOffAccion;
    private InputAction respawn;
    private InputAction saltarAccion;
    private InputAction patadaAccion;
    private InputAction diveAccion;
    private InputAction slideAccion;
    private InputAction viewAccion;
    private InputAction viewOFFAccion;
    private InputAction futbolAccion;
    private InputAction pausarAccion;






    private void Awake()
    {
        mapaActual = PL.currentActionMap;
        moverseAccion = mapaActual.FindAction("Moverse");
        mirarAccion = mapaActual.FindAction("Mirar");
        correrAccion = mapaActual.FindAction("Correr");
        ragdollAccion = mapaActual.FindAction("Ragdoll Trigger");
        ragdollOffAccion = mapaActual.FindAction("Ragdoll Off");
        respawn = mapaActual.FindAction("Respawn");
        saltarAccion = mapaActual.FindAction("Saltar");
        patadaAccion = mapaActual.FindAction("Kick");
        diveAccion = mapaActual.FindAction("Dive");
        slideAccion = mapaActual.FindAction("Slide");
        viewAccion = mapaActual.FindAction("CameraView");
        viewOFFAccion = mapaActual.FindAction("CameraViewOFF");
        futbolAccion = mapaActual.FindAction("KickF");
        pausarAccion = mapaActual.FindAction("Pausa");


        // Cuando la acciones se estan realizando. //
        moverseAccion.performed += enMovimiento;
        mirarAccion.performed += estaMirando;
        correrAccion.performed += estaCorriendo;
        ragdollAccion.performed += debugRagdoll;
        ragdollOffAccion.performed += debugRagdollOff;
        respawn.performed += respawnear;
        saltarAccion.performed += estaSaltando;
        patadaAccion.performed += estaPateando;
        diveAccion.performed += diving;
        slideAccion.performed += sliding;
        viewAccion.performed += cameraChange;
        viewOFFAccion.performed += cameraChangeOFF;
        futbolAccion.performed += estaPateandoF;
        pausarAccion.performed += estaPausando;

        // Cuando la acciones se dejan de realizar. //
        moverseAccion.canceled += enMovimiento;
        mirarAccion.canceled += estaMirando;
        correrAccion.canceled += estaCorriendo;
        ragdollAccion.canceled += debugRagdoll;
        ragdollOffAccion.canceled += debugRagdollOff;
        respawn.canceled += respawnear;
        saltarAccion.canceled += estaSaltando;
        patadaAccion.canceled += estaPateando;
        diveAccion.canceled+= diving;
        slideAccion.canceled+= sliding;
        viewAccion.canceled+= cameraChange;
        viewOFFAccion.canceled+= cameraChangeOFF;
        futbolAccion.canceled += estaPateandoF;
        pausarAccion.canceled += estaPausando;






    }

    private void enMovimiento(InputAction.CallbackContext contexto)
    {
        Moverse = contexto.ReadValue<Vector2>();
    }
    private void estaMirando(InputAction.CallbackContext contexto)
    {
        Mirar = contexto.ReadValue<Vector2>();
    }
    private void estaCorriendo(InputAction.CallbackContext contexto)
    {
        Correr = contexto.ReadValueAsButton();
    }
    private void debugRagdoll(InputAction.CallbackContext contexto)
    {
        RagdollTrigger = contexto.ReadValueAsButton();
    }
    private void debugRagdollOff(InputAction.CallbackContext contexto)
    {
        RagdollTriggerOFF = contexto.ReadValueAsButton();
    }
    private void respawnear(InputAction.CallbackContext contexto)
    {
        Respawn = contexto.ReadValueAsButton();
    }
    private void estaSaltando(InputAction.CallbackContext contexto)
    {
        Salto = contexto.ReadValueAsButton();
    }
    private void estaPateando(InputAction.CallbackContext contexto)
    {
        Patada = contexto.ReadValueAsButton();
    }
    private void diving(InputAction.CallbackContext contexto)
    {
        Dive = contexto.ReadValueAsButton();
    }
    private void sliding(InputAction.CallbackContext contexto)
    {
        Slide = contexto.ReadValueAsButton();
    }
    private void cameraChange(InputAction.CallbackContext contexto)
    {
        CameraView = contexto.ReadValueAsButton();
    }
    private void cameraChangeOFF (InputAction.CallbackContext contexto)
    {
        CameraViewOFF = contexto.ReadValueAsButton();
    }
    private void estaPateandoF(InputAction.CallbackContext contexto)
    {
        PatadaFulbo = contexto.ReadValueAsButton();
    }
    private void estaPausando(InputAction.CallbackContext contexto)
    {
        Pausar = contexto.ReadValueAsButton();
    }

    private void mapaHabilitado()
    {
        mapaActual.Enable();
    }
    private void mapaDeshabilitado()
    {
        mapaActual.Disable();
    }


}
