using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class ControlJugador : MonoBehaviour
{
    private short decoy;
    [Header("---- Variables Iniciales ----\n")]
    [SerializeField] private RagdollActivator ragdoll;
    [SerializeField] private GameManager Manager;
    [SerializeField] private Volume volumen;
    [SerializeField] private ColorAdjustments color;
    [SerializeField] private LensDistortion lens;
    private Rigidbody rb;
    private CapsuleCollider playerCollider;
    private InputManager input;
    private Animator anim;
    [Header("---- Variables Animador----\n")]
    private bool tieneAnimador;
    private int hashVelocidadX;
    private int hashVelocidadY;
    private int saltoHash;
    private int patadaHash;
    private int diveHash;
    [Header("---- Movimiento Jugador ----\n")]
    private const float velocidadCaminar = 2f;
    private const float velocidadCorrer = 6f;
    private Vector2 velocidadActual;
    private float velocidadAnimacion = 8.9f;
    [SerializeField] private float fuerzaSalto = 100f;
    [SerializeField] private float distanciaAlPiso = 1f;
    [SerializeField] internal bool estaPateandoF = false;
    [SerializeField] internal bool nuevoSistemaKick= false;
    [Header("---- Caida Jugador ----\n")]
    public bool estaEnPiso;
    public float tiempoEnAire;
    [SerializeField] public bool caidaNoLetal;
    internal bool animBoolCheck;
    [Header("---- C�mara ----\n")]
    [SerializeField] private Transform CameraRoot3RD;
    [SerializeField] private Transform CameraRoot1ST;
    [SerializeField] private Transform Camera;
    [SerializeField] private float limiteSuperior = -40f;
    [SerializeField] private float limiteInferior = 70f;
    [SerializeField] internal float sensibilidadMouse = 21.9f;
    private float rotacionX;
    [Header("---- CheckPoints----\n")]
    private CheckPoint_Manager checkpoint;
    [Header("---- Menu Pausa ----\n")]
    [SerializeField] private GameObject menuPausa;
    [SerializeField] private PauseManager pausa;
    [SerializeField] internal bool estaEnMenu=false;


    void Start()
    {
        volumen = GameObject.Find("Global Volume").GetComponent<Volume>();
        Manager = GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();
        anim = GetComponent<Animator>();
        animBoolCheck = anim.GetBool("caidaNoLetal");
        playerCollider = gameObject.GetComponent<CapsuleCollider>();
        ragdoll = GameObject.Find("--- Manager Ragdoll ---").GetComponent<RagdollActivator>();
        tieneAnimador = TryGetComponent<Animator>(out anim);
        rb = GetComponent<Rigidbody>();
        input = GetComponent<InputManager>();
        hashVelocidadX = Animator.StringToHash("Velocidad X");
        hashVelocidadY = Animator.StringToHash("Velocidad Y");
        saltoHash = Animator.StringToHash("Salto");
        patadaHash = Animator.StringToHash("Kick");
        patadaHash = Animator.StringToHash("Dive");
        tiempoEnAire = 0;
        checkpoint= GameObject.Find("--- Game Manager ---").GetComponent<CheckPoint_Manager>();
        menuPausa.SetActive(false);
        pausa = GameObject.Find("--- UI Manager ---").GetComponent<PauseManager>();
    }

    private void Update()
    {
        mTiempoEnElAire();
        animBool();
        Pausa();
    }

    private void FixedUpdate()
    {
        Moverse();
        Saltar();
        CheckPiso();
        Patada();
        Dive();
        Slide();
        KickFutbol();
    }
    private void LateUpdate()
    {
        MovimientoCamara();
    }

    private void Moverse()
    {
        if (!tieneAnimador) return;
        if (estaEnMenu == true) return;
        float velocidadTarget = input.Correr? velocidadCorrer : velocidadCaminar;
        if (input.Moverse == Vector2.zero)
        {
            velocidadTarget = 0.1f;
        }
        velocidadActual.x = Mathf.Lerp(velocidadActual.x, input.Moverse.x * velocidadTarget, velocidadAnimacion * Time.fixedDeltaTime);
        velocidadActual.y = Mathf.Lerp(velocidadActual.y, input.Moverse.y * velocidadTarget, velocidadAnimacion * Time.fixedDeltaTime);

        var diferenciaVelocidadX = velocidadActual.x - rb.velocity.x;
        var diferenciaVelocidadZ = velocidadActual.y - rb.velocity.z;


        rb.AddForce(transform.TransformVector(new Vector3(diferenciaVelocidadX, 0, diferenciaVelocidadZ)), ForceMode.VelocityChange);
        anim.SetFloat(hashVelocidadX, velocidadActual.x);
        anim.SetFloat(hashVelocidadY, velocidadActual.y);

    }

    private void CheckPiso()
    {
        if (Physics.Raycast(transform.position, Vector3.down, distanciaAlPiso + 0.1f))
        {
            estaEnPiso = true;
        }
        else
        {
            estaEnPiso = false;
        }
    }

    private void mTiempoEnElAire()
    {
        if (estaEnPiso)
        {
            tiempoEnAire = 0;
        }
        else if (!estaEnPiso && caidaNoLetal==false)
        {
            tiempoEnAire += 1 * Time.deltaTime;
        }
    }

    private void MovimientoCamara()
    {
        if (!tieneAnimador) return;
        if (estaEnMenu==true) return;
        var MouseX = input.Mirar.x;
        var MouseY = input.Mirar.y;
        if(Manager.camara1ST==false)
        Camera.position = CameraRoot3RD.position;
        else
        Camera.position = CameraRoot1ST.position;
        rotacionX -= MouseY * sensibilidadMouse * Time.deltaTime;
        rotacionX = Mathf.Clamp(rotacionX, limiteSuperior, limiteInferior);

        Camera.localRotation = Quaternion.Euler(rotacionX, 0, 0);
        transform.Rotate(Vector3.up, MouseX * sensibilidadMouse * Time.deltaTime);
    }

    private void Saltar()
    {
        if (!tieneAnimador) return;
        if (!input.Salto) return;
        anim.SetTrigger(saltoHash);
    }
    private void Patada()
    {
        if (!tieneAnimador) return;
        if (!input.Patada) return;
        anim.SetTrigger("Kick");
        StartCoroutine(KickingTime());

    }
    private void Dive()
    {
        if (!tieneAnimador) return;
        if (!input.Dive) return;
        anim.SetTrigger("Dive");

    }

    private void Slide()
    {
        if (!tieneAnimador) return;
        if (!input.Slide) return;
        anim.SetTrigger("Slide");
        playerCollider.height = 0.99f;
        StartCoroutine(SlidingTime());
    }

    private void KickFutbol()
    {
        if (!tieneAnimador) return;
        if (!input.PatadaFulbo) return;
        anim.SetTrigger("KickF");
        StartCoroutine(KickingFutbolTime());
    }

    private void Pausa()
    {
        if (!input.Pausar) return;
        menuPausa.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        estaEnMenu = true;
    }
    



    #region EventosAnimaciones

    public void AdicionFuerzaSalto()
    {
        rb.AddForce(-rb.velocity.y * Vector3.up, ForceMode.VelocityChange);
        rb.AddForce(Vector3.up* fuerzaSalto, ForceMode.Impulse);
        anim.ResetTrigger(saltoHash);
    }
    public void RollCrouchON()
    {
        playerCollider.center = new Vector3(0f, 0.89f, 0f);
        playerCollider.direction = 1;
        playerCollider.height = 0.99f;
    }
    public void RollCrouchOFF()
    {
        playerCollider.height = 1.79f;
    }
    public void ZAxisChange()
    {
        playerCollider.direction = 2; // Cambia la direccion del capsule collider del jugador al EJE Z para la caida.
    }

    public void RagdollDiveAnimation()
    {
        playerCollider.center = new Vector3(0f, 1.1f, 0f);
        playerCollider.direction = 2;
        anim.ResetTrigger("Dive");
    }
    public void EstaPateandoPelota()
    {
        if (nuevoSistemaKick == true)
        {
            playerCollider.center = new Vector3(0f, 0.89f, 0.8f);
        }
        estaPateandoF = true;
    }
    public void NoEstaPateandoPelota()
    {
        if (nuevoSistemaKick == true)
        {
            playerCollider.center = new Vector3(0f, 0.89f, 0f);
        }
        estaPateandoF = false;
    }

    #endregion

    private void animBool()
    {
        if (animBoolCheck == false)
            caidaNoLetal = false;
        else
            caidaNoLetal = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ragdoll") == true)
        {
            ragdoll.ragdollActivo = true;
        }
        if (collision.gameObject.CompareTag("Plataforma") == true)
        {
            gameObject.transform.parent = collision.gameObject.transform;
        }
        if (collision.gameObject.CompareTag("TimerEnd") == true)
        {
            Debug.Log("Termino el juego.");
            Manager.terminoTimer = true;
            Manager.camaraAlt = true;
            ragdoll.ragdollActivo = true;
            pausa.Win = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Plataforma") == true)
        {
            gameObject.transform.parent = GameObject.Find("----- ESCENARIO -----").transform;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ragdoll") == true)
        {
            ragdoll.ragdollActivo=true;
        }
        if (other.gameObject.CompareTag("RagdollWater") == true)
        {
            ragdoll.ragdollActivo = true;
            volumen.profile.TryGet<ColorAdjustments>(out color);
            volumen.profile.TryGet<LensDistortion>(out lens);
            color.active = true;
            lens.active = true;

        }
        if (other.gameObject.CompareTag("NonLethalFall") == true)
        {
            animBoolCheck = true;
        }
        if (other.gameObject.CompareTag("Timer") == true)
        {
            Manager.comienzoTimer = true;
            checkpoint.checkP1 = true;
        }
        if (other.gameObject.CompareTag("Pu�o") == true)
        {
            checkpoint.checkP2 = true;
        }
        if (other.gameObject.CompareTag("CHK3") == true)
        {
            checkpoint.checkP3 = true;
        }
        if (other.gameObject.CompareTag("CHK4") == true)
        {
            checkpoint.checkP4 = true;
        }
        if (other.gameObject.CompareTag("CHK5") == true)
        {
            checkpoint.checkP5 = true;
        }
        if (other.gameObject.CompareTag("CHK6") == true)
        {
            checkpoint.checkP6 = true;
        }
        if (other.gameObject.CompareTag("CHK7") == true)
        {
            checkpoint.checkP7 = true;
        }
        if (other.gameObject.CompareTag("chk8") == true)
        {
            checkpoint.checkP8 = true;
        }
        if (other.gameObject.CompareTag("CHK9") == true)
        {
            checkpoint.checkP9 = true;
        }
        if (other.gameObject.CompareTag("CaeRoca") == true)
        {
            InstanciadorPiedra piedra = GameObject.Find("InstanciadorPiedra").GetComponent<InstanciadorPiedra>();
            piedra.caePiedra = true;
        }
        if (other.gameObject.CompareTag("TimerEnd") == true)
        {
            Manager.terminoTimer=true;
            Manager.camaraAlt = true;
            ragdoll.ragdollActivo = true;
            //cartel de victoria
            pausa.Win = true;

        }

    }

    IEnumerator SlidingTime()
    {
        Debug.Log("Comenzo la corrutina SlidingTime");
        yield return new WaitForSecondsRealtime(1.05f);
        playerCollider.height = 1.79f;
        anim.ResetTrigger("Slide");
        yield break;
    }
    IEnumerator KickingTime()
    {
        yield return new WaitForSecondsRealtime(1.20f);
        anim.ResetTrigger("Kick");
        yield break;
    }
    IEnumerator KickingFutbolTime()
    {
        yield return new WaitForSecondsRealtime(1.07f);
        anim.ResetTrigger("KickF");
        yield break;
    }

}
