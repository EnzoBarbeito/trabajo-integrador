using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecieverPelota : MonoBehaviour
{
    [SerializeField] internal Transform[] Spawners;
    [SerializeField] private GameObject botonSpawn;
    private void Start()
    {
        botonSpawn.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Teamgeist")== true )
        {
            other.gameObject.transform.position = Spawners[0].transform.position;
            botonSpawn.SetActive(true);

        }
    }
}
