using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class RagdollWater : MonoBehaviour
{
    [SerializeField] private Volume volumen;
    [SerializeField] private ColorAdjustments color;
    [SerializeField] private LensDistortion lens;
    private void Start()
    {
        volumen = GameObject.Find("Global Volume").GetComponent<Volume>();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("RagdollWater") == true)
        {
            volumen.profile.TryGet<ColorAdjustments>(out color);
            volumen.profile.TryGet<LensDistortion>(out lens);
            color.active = true;
            lens.active = true;
        }
    }
}
