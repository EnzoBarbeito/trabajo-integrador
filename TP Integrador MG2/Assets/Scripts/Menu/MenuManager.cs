using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Header("----- Pantallas de Menu ------\n")]
    [SerializeField] private GameObject StartScreen;
    [SerializeField] private GameObject MenuPrincipal;
    [SerializeField] private GameObject ModeSelect;
    [SerializeField] private GameObject NameColorSelect;
    [SerializeField] private GameObject Opciones;
    [SerializeField] private GameObject Creditos;
    [Header("----- ColorChecks ------\n")]
    [SerializeField] private Toggle clrRojo;
    [SerializeField] private Toggle clrAzul;
    [SerializeField] private Toggle clrAmarillo;
    [SerializeField] private Toggle clrVerde;
    [SerializeField] private Toggle clrBlanco;
    [SerializeField] private Toggle clrNegro;
    [Header("----- ColorChecks ------\n")]
    [SerializeField] private TMPro.TMP_InputField txtNombre;
    [Header("----- Variables Generales ------\n")]
    [SerializeField] private SettingManager setManager;
    [SerializeField] private Toggle camaraALT;

    void Start()
    {
        setManager = GameObject.Find("---SETTINGS MANAGER---").GetComponent<SettingManager>();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        PantallaInicio();
    }

    void Update()
    {
        ColorChecks();
    }


    public void PantallaInicio()
    {
        StartScreen.SetActive(true);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(false);
        Opciones.SetActive(false);
        Creditos.SetActive(false);
    }

    public void PantallaMenuPrincipal()
    {
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(true);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(false);
        Opciones.SetActive(false);
        Creditos.SetActive(false);
    }
    public void PantallaJugadores()
    {
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(true);
        NameColorSelect.SetActive(false);
        Opciones.SetActive(false);
        Creditos.SetActive(false);
    }
    public void PantallaNameColorOG()
    {
        setManager.fcActivo = false;
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(true);
        Opciones.SetActive(false);
        Creditos.SetActive(false);
    }
    public void PantallaNameColor()
    {
        setManager.fcActivo = true;
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(true);
        Opciones.SetActive(false);
        Creditos.SetActive(false);
    }
    public void PantallaOpciones()
    {
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(false);
        Opciones.SetActive(true);
        Creditos.SetActive(false);
    }
    public void PantallaCreditos()
    {
        StartScreen.SetActive(false);
        MenuPrincipal.SetActive(false);
        ModeSelect.SetActive(false);
        NameColorSelect.SetActive(false);
        Opciones.SetActive(false);
        Creditos.SetActive(true);
    }
    public void Salir()
    {
        Application.Quit();
    }

    public void Listo()
    {
        setManager.Nombre = txtNombre.text;
        SceneManager.LoadScene(1);
    }

    private void ColorChecks()
    {
        switch (clrRojo.isOn)
        {
            case true: setManager.Rojo = true;
                break;
            case false: setManager.Rojo = false;
                break;
                
        }
        switch (clrAzul.isOn)
        {
            case true:
                setManager.Azul = true;
                break;
            case false: setManager.Azul = false;
                break;
        }
        switch (clrAmarillo.isOn)
        {
            case true:
                setManager.Amarillo = true;
                break;
            case false:
                setManager.Amarillo = false;
                break;
        }
        switch (clrVerde.isOn)
        {
            case true:
                setManager.Verde= true;
                break;
            case false:
                setManager.Verde = false;
                break;
        }
        switch (clrBlanco.isOn)
        {
            case true:
                setManager.Blanco = true;
                break;
            case false:
                setManager.Blanco = false;
                break;
        }
        switch (clrNegro.isOn)
        {
            case true:
                setManager.Negro = true;
                break;
            case false:
                setManager.Negro = false;
                break;
        }
        switch (camaraALT.isOn)
        {
            case true: setManager.camaraAlt = true;
                break;
            case false: setManager.camaraAlt = false;
                break;
        }
    }

    public void GuardarNombre()
    {
        setManager.Nombre = txtNombre.text;
    }
    public void FChallenge()
    {
        setManager.fcActivo = true;
        PantallaNameColor();
    }
}
