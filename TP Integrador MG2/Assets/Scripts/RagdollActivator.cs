using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class RagdollActivator : MonoBehaviour
{
    private short decoy;
    [Header("---- Variables del Ragdoll -----\n")]
    [SerializeField] private Collider[] colliders;
    [SerializeField] private Rigidbody[] rigidbodies;
    [SerializeField] internal bool ragdollActivo;
    [SerializeField] public float tiempoTriggerRagdollAire;
    //[SerializeField] private bool drunkenCamera;
    [Header("---- Variables del Cuerpo Base -----\n")]
    [SerializeField] private GameObject generalBody;
    [SerializeField] private Rigidbody PlayerRB;
    [SerializeField] private CapsuleCollider colliderJugador;
    [SerializeField] private PlayerInput Inputjugador;
    [SerializeField] private ControlJugador scriptControlJugador;
    [SerializeField] private Animator anim;
    [Header("---- Variables de C�mara-----\n")]
    [SerializeField] private GameObject camara;
    [SerializeField] private GameObject transform_CamaraBody;
    [SerializeField] private GameObject transform_CamaraRagdoll;
    [Header("---- Variables Scripts -----\n")]
    [SerializeField] private GameManager Manager;


    void Start()
    {
        Manager = GameObject.Find("--- Game Manager ---").GetComponent<GameManager>();
        ragdollActivo = false;
        //drunkenCamera = false;
    }

    void Update()
    {
        RagdollAire();
        if (ragdollActivo == true)
        {
            ActivarRagdoll();
        }
        else if (ragdollActivo == false)
        {
            DesactivarRagdoll();
        }

        #region DrunkenCamara
        // ----- FUN DEBUG ONLY ------

        //if (drunkenCamera == true)
        //{
        //    camara.gameObject.transform.parent = transform_CamaraRagdoll.gameObject.transform;
        //}
        //else if (drunkenCamera == false)
        //{
        //    camara.gameObject.transform.parent = transform_CamaraBody.gameObject.transform;

        //}

        // ----- FUN DEBUG ONLY ------
        #endregion

    }
    public void ActivarRagdoll()
    {

        anim.enabled = false;
        PlayerRB.collisionDetectionMode = CollisionDetectionMode.Discrete;
        PlayerRB.constraints = RigidbodyConstraints.FreezePosition;
        PlayerRB.isKinematic= true;

        colliderJugador.enabled = false;
        scriptControlJugador.enabled = false;

        if(Manager.camaraAlt==true)
        camara.transform.LookAt(generalBody.transform);
        else if(Manager.camaraAlt==false)
        camara.gameObject.transform.parent = transform_CamaraRagdoll.gameObject.transform;


        foreach (Collider cl in colliders )
        {
            cl.enabled = true;
            
        }
        foreach(Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = false;
        }

    }
    public void DesactivarRagdoll()
    {
        foreach (Collider cl in colliders)
        {
            cl.enabled = false;
        }
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = true;
        }

        anim.enabled = true;
        PlayerRB.isKinematic = false;
        PlayerRB.collisionDetectionMode = CollisionDetectionMode.Continuous;
        PlayerRB.constraints = RigidbodyConstraints.FreezeRotation;
        colliderJugador.enabled = true;
        //Inputjugador.enabled = true;
        scriptControlJugador.enabled = true;
        camara.gameObject.transform.parent = transform_CamaraBody.gameObject.transform;
        ResetearRotacion();
    }

    private void ResetearRotacion()
    {
        if(generalBody.transform.eulerAngles.x != 0 || generalBody.transform.eulerAngles.z != 0)
        {
            generalBody.transform.eulerAngles = new Vector3(0, gameObject.transform.rotation.y, 0);
        }

    }

    private void RagdollAire()
    {
        if (((scriptControlJugador.tiempoEnAire >= tiempoTriggerRagdollAire)&& scriptControlJugador.estaEnPiso==true) || scriptControlJugador.tiempoEnAire >= tiempoTriggerRagdollAire*2)
        {
            ragdollActivo = true;
        }

    }
}
